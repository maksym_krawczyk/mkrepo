using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MkApp.Core.Models;
using MkApp.Core.Services.Contracts;
using MkApp.Core.ViewModels.Home;
using Moq;
using NUnit.Framework;

namespace MkApp.Core.Test
{
    [TestFixture]
    public class HomeViewModelTests
    {
        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>();
        private readonly Mock<ILogger> _loggerMock = new Mock<ILogger>();

        [Test]
        public async Task Initialize_Always_ShouldCallGetUsers()
        {
            //Arrange
            _userServiceMock.Setup(x => x.GetUsers()).ReturnsAsync(new List<UserModel>());
            HomeViewModel sut = CreateSut();

            //Act
            await sut.Initialize();

            //Assert
            _userServiceMock.Verify(x => x.GetUsers());
        }

        [Test]
        public async Task Initialize_Always_ShouldFetchCorrectUserData()
        {
            //Arrange
            var expectedList = new List<UserModel>();
            expectedList.Add(new UserModel() { FirstName = "user1"});
            expectedList.Add(new UserModel() { FirstName = "user2"});
            expectedList.Add(new UserModel() { FirstName = "user3"});
            _userServiceMock.Setup(x => x.GetUsers()).ReturnsAsync(expectedList);
            HomeViewModel sut = CreateSut();

            //Act
            await sut.Initialize();

            //Assert
            Assert.AreEqual(expectedList.Count, sut.UsersCollection.Count);
        }

        [Test]
        public async Task Initialize_ThrowsException_ShouldChangeIsLoadingToFalse()
        {
            //Arrange
            HomeViewModel sut = CreateSut();
            _userServiceMock.Setup(x => x.GetUsers()).Throws<Exception>();

            //Act
            await sut.Initialize();

            //Assert
            Assert.IsFalse(sut.IsLoading);
        }

        private HomeViewModel CreateSut()
        {
            return new HomeViewModel(_userServiceMock.Object, _loggerMock.Object);
        }
    }
}
