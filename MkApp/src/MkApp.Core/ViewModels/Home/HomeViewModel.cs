using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MkApp.Core.Models;
using MkApp.Core.Services.Contracts;
using MvvmCross.ViewModels;

namespace MkApp.Core.ViewModels.Home
{
    public class HomeViewModel : BaseViewModel
    {
        private readonly IUserService _userService;
        private readonly ILogger _logger;
        private bool _isLoading;

        public HomeViewModel(IUserService userService, ILogger logger)
        {
            _userService = userService;
            _logger = logger;

            InitializeTask = MvxNotifyTask.Create(LoadUsersAsync);
        }

        public MvxObservableCollection<UserModel> UsersCollection { get; } = new MvxObservableCollection<UserModel>();

        public bool IsLoading
        {
            get => _isLoading;
            set => SetProperty(ref _isLoading, value);
        }

        private async Task LoadUsersAsync()
        {
            try
            {
                IsLoading = true;

                UsersCollection.AddRange(await _userService.GetUsers());
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}
