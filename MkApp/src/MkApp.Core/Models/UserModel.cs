using System.Text.Json.Serialization;

namespace MkApp.Core.Models
{
    public class UserModel
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }

        [JsonPropertyName("lastName")]
        public string LastName { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("gender")]
        public string Gender { get; set; }

        [JsonPropertyName("ipAddress")]
        public string IpAddress { get; set; }


        public override string ToString() => $"{FirstName} {LastName}";
    }
}
