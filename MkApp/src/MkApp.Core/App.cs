﻿using Microsoft.Extensions.Logging;
using MkApp.Core.Services.Contracts;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using MkApp.Core.ViewModels.Home;
using MvvmCross;
using MvvmCross.Logging;

namespace MkApp.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.IoCProvider.RegisterType(() => MvxLogHost.Default);
            Mvx.IoCProvider.Resolve<IApiConfigService>().RegisterApis();

            RegisterAppStart<HomeViewModel>();
        }
    }
}
