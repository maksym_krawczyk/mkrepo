using System.Collections.Generic;
using System.Threading.Tasks;
using MkApp.Core.Models;
using MkApp.Core.Networking.Api.User;
using MkApp.Core.Services.Contracts;

namespace MkApp.Core.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IUserApi _userApi;

        public UserService(IUserApi userApi)
        {
            _userApi = userApi;
        }

        public async Task<IEnumerable<UserModel>> GetUsers() => await _userApi.GetUsers();
    }
}
