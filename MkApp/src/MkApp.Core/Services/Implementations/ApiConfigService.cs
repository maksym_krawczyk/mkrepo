using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using MkApp.Core.Networking.Api.User;
using MkApp.Core.Networking.Config;
using MkApp.Core.Services.Contracts;
using MkApp.Core.Settings;
using MvvmCross;
using MvvmCross.Binding;
using Refit;

namespace MkApp.Core.Services.Implementations
{
    public class ApiConfigService : IApiConfigService
    {
        private readonly ILogger _logger;
        private readonly HttpClient _client;

        public ApiConfigService(ILogger logger)
        {
            _logger = logger;
            _client = new HttpClient(new CustomHttpHandler(), true)
            {
                Timeout = TimeSpan.FromSeconds(BuildConstants.Api.RequestTimeout),
                BaseAddress = new Uri(BuildConstants.Api.BaseAddress)
            };

            // _client.DefaultRequestHeaders.Add("X-API-Key", "58c0ae40");
        }

        public void RegisterApis()
        {
            RegisterApi<IUserApi>();
        }

        private void RegisterApi<TApi>() where TApi : class
        {
            try
            {
                Mvx.IoCProvider.RegisterType(() => RestService.For<TApi>(_client));
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
            }
        }
    }
}
