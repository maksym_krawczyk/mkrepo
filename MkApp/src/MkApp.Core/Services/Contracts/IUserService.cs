using System.Collections.Generic;
using System.Threading.Tasks;
using MkApp.Core.Models;

namespace MkApp.Core.Services.Contracts
{
    public interface IUserService
    {
        Task<IEnumerable<UserModel>> GetUsers();
    }
}
