namespace MkApp.Core.Services.Contracts
{
    public interface IApiConfigService
    {
        void RegisterApis();
    }
}
