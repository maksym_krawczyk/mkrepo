namespace MkApp.Core.Settings
{
    public static class BuildConstants
    {
        public static class Api
        {
            public const int RequestTimeout = 30;
            public const string BaseAddress = "https://my.api.mockaroo.com";
        }
    }
}
