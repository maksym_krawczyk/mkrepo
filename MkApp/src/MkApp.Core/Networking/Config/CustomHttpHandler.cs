using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace MkApp.Core.Networking.Config
{
    public class CustomHttpHandler : HttpClientHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Console.WriteLine($"------------------------ Request: {DateTime.Now} -----------------------");
            Console.WriteLine(request);
            if (request?.Content != null)
                Console.WriteLine(await request.Content.ReadAsStringAsync());

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            Console.WriteLine($"------------------------ Response: {DateTime.Now} -----------------------");
            Console.WriteLine(response);
            if (response?.Content != null)
                Console.WriteLine(await response.Content.ReadAsStringAsync());

            return response;
        }
    }
}
