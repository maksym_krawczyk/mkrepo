using System.Collections.Generic;
using System.Threading.Tasks;
using MkApp.Core.Models;
using Refit;

namespace MkApp.Core.Networking.Api.User
{
    public interface IUserApi
    {
        [Get("/users?key=58c0ae40")]
        Task<List<UserModel>> GetUsers();
    }
}
